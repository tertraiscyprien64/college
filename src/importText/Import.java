package importText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dataCollect.Api;
import models.Classe;
import models.Eleve;
import models.Niveau;
import org.json.JSONObject;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Import {

    public static void main(String[] args) throws IOException {
        Import.readFile();


    }

    /**
     * Permet de lire le fichier d'importation
     * @throws IOException
     */
    static void readFile() throws IOException {
        File file = new File("import.txt");

        BufferedReader br = new BufferedReader(new FileReader(file));
        Api api = new Api();
        api.load("save.json");

        String st;

        String matiere="";
        Classe c = new Classe("", new ArrayList<>());
        int idClasse=0;
        int idNiveau=0;
        boolean present=true;
        int cpt=0;
        while ((st = br.readLine()) != null) {


            if (cpt == 0) {

                for (int i = 0; i < api.getNiveaux().size(); i++) {
                    if (api.getNiveaux().get(i).getNiveau().equals(st.charAt(0) + "")) {

                        idNiveau=i;
                    }
                }

                for (int y = 0; y < api.getNiveaux().get(idNiveau).getClasses().size(); y++) {
                    if (api.getNiveaux().get(idNiveau).getClasses().get(y).getLettre().equals(st.charAt(1) + "")) {
                        c = api.getNiveaux().get(idNiveau).getClasses().get(y);
                        idClasse=y;
                        break;
                    } else {

                    }
                }
                if (c.getLettre().equals("")) {
                    api.getNiveaux().get(idNiveau).getClasses().add(new Classe(st.charAt(1) + "", new ArrayList<>()));
                    idClasse=api.getNiveaux().get(idNiveau).getClasses().size()-1;
                }


            }
            if(cpt==1){
                String [] tab={"MATHS","FRANCAIS","LATIN","GREC","ANGLAIS","ART","MUSIQUE","SPORT","PHYSIQUE","SVT","HISTOIRE-GEO","LANGUE","ANGLAIS_AVANCE"};
                for(int i=0;i<tab.length;i++){

                    if(st.toUpperCase().equals(tab[i])){
                        matiere=tab[i];
                    }
                }

            }
            if(cpt>=2){
                if( isNumeric(st.charAt(0)+"")){

                    for (int i = 0; i < api.getNiveaux().size(); i++) {
                        if (api.getNiveaux().get(i).getNiveau().equals(st.charAt(0) + "")) {

                        }
                    }

                    for (int y = 0; y < api.getNiveaux().get(idNiveau).getClasses().size(); y++) {
                        if (api.getNiveaux().get(idNiveau).getClasses().get(y).getLettre().equals(st.charAt(1) + "")) {
                            c = api.getNiveaux().get(idNiveau).getClasses().get(y);
                            break;
                        } else {

                        }
                    }
                    if (c.getLettre().equals("")) {
                        api.getNiveaux().get(idNiveau).getClasses().add(new Classe(st.charAt(1) + "", new ArrayList<>()));
                    }


                    cpt=1;
                }else {

                    String nom = st.substring(0, st.indexOf(" "));
                    String prenom = st.substring(st.indexOf(" ") + 1).substring(0, st.substring(st.indexOf(" ") + 1).indexOf(" "));
                    System.out.println("&&&&"+st+"&&&&");
                    double note = Double.parseDouble(st.substring(st.lastIndexOf(" ")));
                    System.out.println(note);
                    List<Double> l = new ArrayList<>();
                    l.add(note);
                    HashMap<String, List> map = new HashMap<String, List>();
                    map.put(matiere, l);
                    Eleve e = new Eleve(nom, prenom, map);
                    api.getNiveaux().get(idNiveau).getClasses().get(idClasse).getEleves().add(e);
                }

            }
            cpt++;
        }
        Gson gson = new GsonBuilder().create();
        System.out.println("Importation reussie ;)");

        update(gson.toJson(api.getNiveaux()));
    }

    /**
     * Peremt de faire un update du fichier save.json
     * @param s
     */
    static void update(String s) {



        Path path = null;
        try {
            path = Files.createTempFile("save", ".json");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (Files.exists(path)) {
            FileWriter fileWriter = null;

            try {
                fileWriter = new FileWriter("save.json");
            } catch (IOException e) {
                e.printStackTrace();
            }
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.print(s);
            printWriter.close();

        } else {
            System.out.println("Fichier n'existe pas");
        }
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
}



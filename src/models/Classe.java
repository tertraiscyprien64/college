package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Classe {
    private String lettre;
    private List<Eleve> eleves;

    public Classe(String lettre, List<Eleve> eleves) {
        this.lettre = lettre;
        this.eleves = eleves;

    }
    public Classe(String lettre, List<Eleve> eleves,String niveau) {
        this.lettre = lettre;
        this.eleves = eleves;
        int i=0;
        while(i<20){
            eleves.add(new Eleve(niveau+lettre+"yaya"+i,"toto"+i,genMatiere(niveau)));
            i++;
        }
    }

    public String getLettre() {
        return lettre;
    }

    public void setLettre(String lettre) {
        this.lettre = lettre;
    }

    public List<Eleve> getEleves() {
        return eleves;
    }

    public void setEleves(List<Eleve> eleves) {
        this.eleves = eleves;
    }

    /**
     * Permet de générer une liste de notes avec une taille en param
     * @param taille
     * @return
     */

    List<Double> genNotes(int taille){
        int i=0;
        List<Double> notes= new ArrayList<>();
        Random r = new Random();
        while(i<taille){
    notes.add((20 ) * r.nextDouble());
    i++;
        }
        return notes;
    }

    /**
     * Permet de générer les matères d'un eleve
     * @param niveau
     * @return
     */
    HashMap<String,List> genMatiere(String niveau){
        HashMap<String,List> map= new HashMap<>();
        map.put("FRANCAIS",genNotes(3));
        map.put("MATHS",genNotes(3));
        map.put("ANGLAIS",genNotes(3));
        map.put("HISTOIRE-GEO",genNotes(3));
        map.put("SVT",genNotes(3));
        map.put("ART",genNotes(3));
        map.put("MUSIQUE",genNotes(2));
        map.put("SPORT",genNotes(2));
       if(!niveau.equals("6ème")){
           map.put("LANGUE",genNotes(3));
           map.put("PHYSIQUE",genNotes(3));

       }
       /*
       generation de matieres facultatives

        */
       String[] faculative={"LATIN","GREC","ANGLAIS_AVANCE"};
        Random rand = new Random();
        int nombreAleatoire = rand.nextInt(2 - 0 + 1);
        if(nombreAleatoire==1){
            int choix= rand.nextInt(2 - 0 + 1);
            map.put(faculative[choix],genNotes(3));
        }else if(nombreAleatoire==2){
            int choix= rand.nextInt(2 - 0 + 1);
            if(choix==0 || choix ==1){
                map.put(faculative[choix],genNotes(3));
                map.put(faculative[2],genNotes(3));
            }else{
                map.put(faculative[2],genNotes(3));
                choix= rand.nextInt(1 - 0 + 1);
                map.put(faculative[choix],genNotes(3));

            }

        }


    return map;
    }

    /**
     * Retroune une liste de matiere par moyenne
     * @param matiere
     * @return
     */
    public List<Double> moyennes(String matiere){
        List<Double> l = new ArrayList<>();
        for(int i=0;i<this.eleves.size();i++){
            double moyenne=0.0;
            if(this.eleves.get(i).getMatieres().containsKey(matiere)) {
                for (int y = 0; y < this.eleves.get(i).getMatieres().get(matiere).size(); y++) {
                    moyenne += (double) this.eleves.get(i).getMatieres().get(matiere).get(y) / this.eleves.get(i).getMatieres().get(matiere).size();
                }
                l.add(moyenne);
            }
        }
        return l;
    }

}

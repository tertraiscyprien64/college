package models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.*;

public class Eleve {
    private HashMap<String,List> matieres;
    private HashMap<String,List> matieresFaculatives;
    private String nom;
    private String prenom;

    public Eleve(String nom, String prenom,HashMap<String,List> matieres) {
        this.matieres = matieres;
        this.nom = nom;
        this.prenom = prenom;
    }

    public HashMap<String, List> getMatieres() {
        return matieres;
    }

    public void setMatieres(HashMap<String, List> matieres) {
        this.matieres = matieres;
    }

    public HashMap<String, List> getMatieresFaculatives() {
        return matieresFaculatives;
    }

    public void setMatieresFaculatives(HashMap<String, List> matieresFaculatives) {
        this.matieresFaculatives = matieresFaculatives;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Permet de calculer la moyenne générale d'un élève
     * @return
     */

    public double getMoyenneGenerale(){
        double moyenne=0.0;
        double bonus=0.0;
        int cpt=0;
        Iterator<Map.Entry<String,List>> it = this.getMatieres().entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<String, List> entry = it.next();
            String key = entry.getKey();
            List l = entry.getValue();
            if(key.equals("LATIN") || key.equals("GREC") || key.equals("ANGLAIS_AVANCE")){
                double moy=0.0;
                for(int i=0;i<l.size();i++){
                    moy+=(double)l.get(i)/l.size();
                }
               bonus= (Math.round(moy)-10)/10;
                cpt++;
            }else{
                for(int i=0;i<l.size();i++){
                    moyenne+=(double)l.get(i)/l.size();
                }
            }


        }
        return (moyenne/(this.getMatieres().entrySet().size()-cpt))+bonus;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }
}

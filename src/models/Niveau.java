package models;

import java.util.ArrayList;
import java.util.List;

public class Niveau {
    private List<Classe> classes;
    private String niveau;
    public Niveau(String niveau){
        this.niveau=niveau;
        this.classes=new ArrayList<>();
        this.classes.add(new Classe("A",new ArrayList<>(),this.niveau));
        this.classes.add(new Classe("B",new ArrayList<>(),this.niveau));
        this.classes.add(new Classe("C",new ArrayList<>(),this.niveau));
        this.classes.add(new Classe("D",new ArrayList<>(),this.niveau));
        this.classes.add(new Classe("E",new ArrayList<>(),this.niveau));
        this.classes.add(new Classe("F",new ArrayList<>(),this.niveau));
    }

    public List<Classe> getClasses() {
        return classes;
    }

    public void setClasses(List<Classe> classes) {
        this.classes = classes;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }
}

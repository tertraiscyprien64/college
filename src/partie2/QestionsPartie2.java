package partie2;

import dataCollect.Api;

public class QestionsPartie2 {
    public static void main(String[] args){
        Api api = new Api();
        api.load("save.json");


        /*
        Toutes les classes d'un niveau 1======
         */
        System.out.println(api.getAllMark("5èmeB"));

        /*
        Toutes les classes d'un niveau 2======
         */
        System.out.println(api.getAllMarkStudent("3èmeAyaya0"));


        /*
        Toutes les classes d'un niveau 3==========
         */

        System.out.println(api.getAllMarkLevel("3ème"));


        /*
        Question 4==========
         */


        System.out.println(api.getMarkInfo(true));
    }
}

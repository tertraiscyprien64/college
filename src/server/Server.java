package server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dataCollect.Api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

/**
 * Load api pour les envoyers aux clients
 */
public class Server {
    private ServerSocket serverSocket;
    static  Api api = new Api();
    public Server(){
        this.api=new Api();
        api.load("save.json");
    }

    public static void main(String args[]) throws IOException {
       Server s = new Server();
       s.start(1234);


    }
    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);


        while (true)
            new ServerRun(serverSocket.accept()).start();
    }

    /**
     * Multithread pour plusieurs client permet de lancer un thread serveur
     */
    private static class ServerRun extends Thread {
        private Socket clientSocket;
        private PrintWriter out;
        private BufferedReader in;

        public ServerRun(Socket socket) {
            this.clientSocket = socket;

        }

        /**
         * Permet de lancer le socket et de repondre au client  ( avec des patern specifics en message via appel d'API et envoi en JSON)
         */
        public void run() {
            try {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }

            String inputLine="";
            while (true) {
                try {
                    if (!((inputLine = in.readLine()) != null)) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.print(new Date().toString()+" "+inputLine+"    ");
                if(inputLine.equals("/getAllMarks")){
                    out.println(api.getMarkInfo());
                    break;
                }else if(inputLine.matches("/getAllMarksFromLevel/.*/.*")){
                    String matiere= inputLine.substring(inputLine.indexOf("/",1)+1,inputLine.lastIndexOf('/'));

                    int id= Integer.parseInt(inputLine.substring(inputLine.lastIndexOf('/')+1));


                    out.println(api.getAllMarkFromLevel(matiere,id));
                    System.out.println(200);
                }else if(inputLine.matches("/getClasse/.*")){
                    String classe= inputLine.substring(inputLine.indexOf("/",1)+1);

                    Gson gson= new GsonBuilder().create();
                    out.println(gson.toJson(api.getClasse(classe)));
                    System.out.println(200);
                }else{
                    Gson gson= new GsonBuilder().create();
                    out.println(gson.toJson(new ArrayList<>()));
                    System.out.println(400);
                }

            }

            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.close();
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }}
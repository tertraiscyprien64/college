package server;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sun.awt.windows.ThemeReader;

public class Main extends Application {
    /**
     * Lance l'interface graphique mais attend que le serveur soit lancé ;)
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{


        Client c= new Client();
        primaryStage.setTitle("Notes avec serveur");

        while(!c.canReach("127.0.0.1",1234)){
            System.out.println("Waiting the server to start on 1234 port localhost====Please start it");
            Thread.sleep(2000);
        }
            Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
            primaryStage.setScene(new Scene(root, 1200, 800));

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

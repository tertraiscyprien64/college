package sample;

import dataCollect.Api;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import models.Classe;
import org.json.JSONObject;

import java.net.URL;
import java.util.*;

public class Controller implements Initializable {
    @FXML
    private ComboBox<String> choixMatiere;
    @FXML
    private ComboBox<String> choixNiveau;
    @FXML
    private ComboBox<String> choix12;
    @FXML
    private ComboBox<Integer> choix22;
    @FXML
    private ComboBox<String> choix3;
    @FXML
    private BarChart<String,Double> GRAPH1;
    @FXML
    private LineChart<String,Double> GRAPH2;
    @FXML
    private LineChart<String,Double> GRAPH3;
    @FXML
    private LineChart<String,Double> GRAPH4;
    @FXML
    private ComboBox<String> choixNivGraph2;
    @FXML
    private ComboBox<String> choixNivGraph3;
    @FXML
    private ComboBox<String> choixNivGraph4;

    private Api api = new Api();

    /**
     * Permet d'inililiser toutes les combo et les graphes
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        api.load("save.json");
        ObservableList<String> options = FXCollections.observableArrayList("MATHS", "ANGLAIS", "FRANCAIS", "ART", "MUSIQUE", "SPORT", "HISTOIRE-GEO", "SVT", "ANGLAIS_AVANCE", "LATIN", "GREC", "PHYSIQUE", "LANGUE");
        ObservableList<String> options1 = FXCollections.observableArrayList("3ème", "4ème", "5ème", "6ème");
        choixMatiere.setItems(options);
        choixMatiere.getSelectionModel().select("MATHS");
        choixNiveau.setItems(options1);
        choixNivGraph2.setItems(options1);
        choixNivGraph2.getSelectionModel().select("3ème");
        choixNivGraph3.setItems(options1);
        choixNivGraph3.getSelectionModel().select("3ème");
        choixNivGraph4.setItems(options1);
        choixNivGraph4.getSelectionModel().select("3ème");

        choixNiveau.getSelectionModel().select("3ème");
        ObservableList<Integer> options21 = FXCollections.observableArrayList(1, 2, 3);
        choix12.setItems(options);
        choix12.getSelectionModel().select("MATHS");
        choix22.setItems(options21);
        choix22.getSelectionModel().select(1);
        choix3.setItems(options);
        choix3.getSelectionModel().select("MATHS");
        generateGraph1(choixMatiere.getSelectionModel().getSelectedItem(),choixNiveau.getSelectionModel().getSelectedItem());
        generateGraph2(choix12.getSelectionModel().getSelectedItem(),choix22.getSelectionModel().getSelectedItem(),choixNivGraph2.getSelectionModel().getSelectedItem());
        generateGraph3(choix3.getSelectionModel().getSelectedItem(),choixNivGraph3.getSelectionModel().getSelectedItem());
        generateGraph4(choixNivGraph4.getSelectionModel().getSelectedItem());

    }

    /**
     * On action sur les combo graph1
     */
    public void comboAction() {

        if(!choixNiveau.getValue().isEmpty() && !choixMatiere.getValue().isEmpty()){
            GRAPH1.getData().clear();
            generateGraph1(choixMatiere.getValue(),choixNiveau.getValue());
        }
    }
    /**
     * On action sur les combo graph2
     */
    public void comboAction2() {

        if(!choix12.getValue().isEmpty() && !choix22.getValue().toString().isEmpty()){
            GRAPH2.getData().clear();
            generateGraph2(choix12.getValue(),choix22.getValue(),choixNivGraph2.getValue());
        }
    }
    /**
     * On action sur les combo graph3
     */
    public void comboAction3() {
        GRAPH3.getData().clear();
        generateGraph3(choix3.getValue(),choixNivGraph3.getValue());
    }
    /**
     * On action sur les combo graph4
     */
    public void comboAction4() {
        GRAPH4.getData().clear();
        generateGraph4(choixNivGraph4.getSelectionModel().getSelectedItem());
    }

    /**
     * genere le graph 1  via l'API
     */
    void generateGraph1(String matiere,String niveau){
        String json= api.getMarkInfo();
        JSONObject jo = new JSONObject(json);
        HashMap<String, Double> l = new HashMap<>();
        char idx='A';
        while(idx<='F'){
            Double moyenne= 0.0;
            for (int i=0;i<jo.getJSONObject(niveau+idx).getJSONArray(matiere).length();i++){
                moyenne+=(Double) jo.getJSONObject(niveau+idx).getJSONArray(matiere).get(i)/jo.getJSONObject(niveau+idx).getJSONArray(matiere).length();
            }
            l.put(niveau+idx,moyenne);
            idx++;
        }
        XYChart.Series<String, Double> series = new XYChart.Series<>();
        for (Map.Entry<String, Double> entry : l.entrySet()) {
            series.getData().add(new XYChart.Data<>(entry.getKey(), entry.getValue()));
        }
        GRAPH1.getData().add(series);

    }
    /**
     * genere le graph 2  via l'API
     */
    void generateGraph2(String matiere,int id,String level){
        id--;
        String json= api.getAllMarkFromLevel(matiere,id);


        JSONObject jo = new JSONObject(json);
        HashMap<String, Double> l = new HashMap<>();
        GRAPH2.setTitle("Note numero "+(id+1)+"de l'epreuve "+matiere);



            char idx='A';
            while(idx<='F'){
                int i = Integer.parseInt(level.charAt(0)+"");
                XYChart.Series series = new XYChart.Series();
                series.setName(i+"ème"+idx);

                if(jo.has(i+"ème"+idx)) {
                    int tab[] = new int[21];
                    for (int j = 0; j < jo.getJSONArray(i + "ème" + idx).length(); j++) {
                    int val= (int) Math.round((double) jo.getJSONArray(i + "ème" + idx).get(j));
                    tab[val]=tab[val]+1;
                    }
                    for (int j = 0; j < 20; j++) {

                        series.getData().add(new XYChart.Data((j + 1 + ""),tab[j]));
                    }
                }
                GRAPH2.getData().add(series);



                idx++;
        }

    }

    /**
     * genere le graph 3  via l'API
     */
    void generateGraph3(String matiere,String level){
       //System.out.println(api.getAllMark("3èmeB"));
        //String json= api.getAllMarkFromLevel(matiere,id);
        char idx='A';
        while(idx<='F'){
            int i = Integer.parseInt(level.charAt(0)+"");
                XYChart.Series series = new XYChart.Series();
                series.setName(i+"ème"+idx);
                Classe c = api.getClasse(i+"ème"+idx);
                List<Double> l = c.moyennes(matiere);

                    int tab[] = new int[21];
                    for (int j = 0; j < l.size(); j++) {
                        int val= (int) Math.round((double) l.get(j));
                        tab[val]=tab[val]+1;
                    }
                    for (int j = 0; j < 20; j++) {

                        series.getData().add(new XYChart.Data((j + 1 + ""),tab[j]));
                    }
                GRAPH3.getData().add(series);

                idx++;
            }

        }
    /**
     * genere le graph 4  via l'API
     */
    void generateGraph4(String level){


        char idx='A';
        while(idx<='F'){
                int i = Integer.parseInt(level.charAt(0)+"");
                XYChart.Series series = new XYChart.Series();
                series.setName(i+"ème"+idx);
                Classe c = api.getClasse(i+"ème"+idx);
                List<Double> l = new ArrayList<>();
                for(int u=0;u<c.getEleves().size();u++){
                l.add(c.getEleves().get(u).getMoyenneGenerale());
                }

                int tab[] = new int[21];
                for (int j = 0; j < l.size(); j++) {
                    int val= (int) Math.round((double) l.get(j));
                    tab[val]=tab[val]+1;
                }
                for (int j = 0; j < 20; j++) {

                    series.getData().add(new XYChart.Data((j + 1 + ""),tab[j]));
                }
                GRAPH4.getData().add(series);

            idx++;
        }

    }

    }




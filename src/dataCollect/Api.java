package dataCollect;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.Classe;
import models.Eleve;
import models.Niveau;
import org.json.JSONObject;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;


public class Api {
    private List<Niveau> niveaux= new ArrayList<>();


    public String getAllMark(String choix) {

        return new JSONObject().put("Notes", getClasse(choix).getEleves()).toString();
    }

    public String getAllMarkLevel(String level) {


        return new JSONObject().put("Niveau", getNiveaubyID(level).getClasses()).toString();
    }


    /**
     * Cette fonction permet de retourner toutes les informations utiles pour avec les information sur les notes
     * @param affiche a true permet d'afficher mediane moyenne min max
     * @return
     */
    public String getMarkInfo(boolean ... affiche){

        HashMap<String,HashMap<String,List>> allMark= new HashMap<>();
        for(int j=0;j<this.niveaux.size();j++){
            for(int y=0;y<this.niveaux.get(j).getClasses().size();y++){
                HashMap<String,List> notesclasses=new HashMap<>();
                for(int f=0;f<this.niveaux.get(j).getClasses().get(y).getEleves().size();f++){

                   Iterator<Map.Entry<String,List>> itr = this.niveaux.get(j).getClasses().get(y).getEleves().get(f).getMatieres().entrySet().iterator();
                    while(itr.hasNext()) {
                        Map.Entry<String, List> entry = itr.next();
                        String key = entry.getKey();
                        List<Double> value = entry.getValue();
                        if(!notesclasses.containsKey(key)) {
                            notesclasses.put(key, value);
                        }else{
                            List<Double> l= notesclasses.get(key);
                            if(l.addAll(value)){
                                notesclasses.put(key,l);
                            }
                        }
                    }

                }
                Iterator<Map.Entry<String,List>> itr2 = notesclasses.entrySet().iterator();

                HashMap<String,List> maps= new HashMap<>();
                while(itr2.hasNext()) {
                    Map.Entry<String, List> entry = itr2.next();
                    String key = entry.getKey();
                    List<Double> value = entry.getValue();
                    if(value.size()!=40){

                        double note1=0;
                        double note2=0;
                        double note3=0;
                        List<Double> l1 =new ArrayList<>();
                        List<Double> l2=new ArrayList<>();;
                        List<Double> l3=new ArrayList<>();;
                        for(int i=0;i<value.size();i++){

                            if(i%3==0){
                                l1.add(value.get(i));
                                note1+=value.get(i)/(value.size()/3);
                            }else if(i%3==1){
                                l2.add(value.get(i));
                                note2+=value.get(i)/(value.size()/3);
                            }else if(i%3==2){
                                l3.add(value.get(i));
                                note3+=value.get(i)/(value.size()/3);
                            }

                        }
                        Collections.sort(l1);
                        Collections.sort(l2);
                        Collections.sort(l3);
                        if( affiche.length>0 && affiche[0]){
                            System.out.println(this.niveaux.get(j).getNiveau()+this.niveaux.get(j).getClasses().get(y).getLettre()+""+key+"  MIN=="+l1.get(0)+"  MIN==="+l2.get(0)+"  MIN=="+l3.get(0)+"  MAX=="+l1.get(l1.size()-1)+"  MAX==="+l2.get(l2.size()-1)+"  MAX=="+l3.get(l3.size()-1)+"MED=="+l1.get((l1.size()-1)/2)+"  MED==="+l2.get((l2.size()-1)/2)+"  MED=="+l3.get((l3.size()-1)/2));

                        }

                        maps.put(key,Arrays.asList(note1,note2,note3) );
                    }else if(value.size()==40){
                        double note1=0;
                        double note2=0;
                        for(int i=0;i<value.size();i++){
                            if(i%2==0){
                                note1+=value.get(i)/20;
                            }else if(i%2==1){
                                note2+=value.get(i)/20;
                            }
                        }
                        maps.put(key,Arrays.asList(new Double(note1),new Double(note2)) );
                    }

                }

                allMark.put(this.niveaux.get(j).getNiveau()+this.niveaux.get(j).getClasses().get(y).getLettre(),maps);
            }
        }


        Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();
        return gson.toJson(allMark);
    }

    /**
     * Permet de retrouner un ensemble de notes d'une matière specifique et epreuve specifique
     * @param matiere
     * @param id
     * @return
     */
    public String getAllMarkFromLevel(String matiere,int id){
        HashMap<String,List> map= new HashMap<>();
        for(int j=0;j<this.niveaux.size();j++) {
            Niveau n  = this.niveaux.get(j);

            for (int i = 0; i < n.getClasses().size(); i++) {
                List<Double> notes = new ArrayList<>();
                for (int y = 0; y < n.getClasses().get(i).getEleves().size(); y++) {
                    if (n.getClasses().get(i).getEleves().get(y).getMatieres().containsKey(matiere) && id < n.getClasses().get(i).getEleves().get(y).getMatieres().get(matiere).size()) {
                        if( id==2 && (matiere.equals("SPORT") || matiere.equals("MUSIQUE"))){

                        }else {
                            notes.add((Double) n.getClasses().get(i).getEleves().get(y).getMatieres().get(matiere).get(id));
                        }
                        map.put(n.getNiveau() + n.getClasses().get(i).getLettre(), notes);
                    }
                }


            }
        }
        Gson gson=new GsonBuilder().create();

       return gson.toJson(map).toString();
    }

    /**
     * Retourne toutes les notes d'un élève
     * @param eleve
     * @return
     */
    public String getAllMarkStudent(String eleve) {
        JSONObject jo = new JSONObject();
        Eleve student= new Eleve("","",null);
        for(int i=0;i<niveaux.size();i++){
            for(int y=0;y<niveaux.get(i).getClasses().size();y++){
                for(int j=0;j<niveaux.get(i).getClasses().get(y).getEleves().size();j++){
                    if(niveaux.get(i).getClasses().get(y).getEleves().get(j).getNom().equals(eleve)){
                        return niveaux.get(i).getClasses().get(y).getEleves().get(j).toString();
                    }

                }


            }
        }




        return "";

    }

    /**
     * Permet de retourner une classe en fonction de son niveau et de sa lettre : exemple 6ème
     * @param niveau
     * @return
     */
    public Classe getClasse(String niveau) {
        if (niveau.length() != 5) {
            return null;
        }
        int idxNiv = 0;
        int classe = 0;
        char level = niveau.charAt(0);
        char lettre = niveau.charAt(4);
        if (level == '6') {
            idxNiv = 0;
        } else if (level == '5') {
            idxNiv = 1;
        } else if (level == '4') {
            idxNiv = 2;
        } else if (level == '3') {
            idxNiv = 3;
        } else {
            return null;
        }
        if (lettre == 'A') {
            classe = 0;
        } else if (lettre == 'B') {
            classe = 1;
        } else if (lettre == 'C') {
            classe = 2;
        } else if (lettre == 'D') {
            classe = 3;
        } else if (lettre == 'E') {
            classe = 4;
        } else if (lettre == 'F') {
            classe = 5;
        } else {
            return null;
        }
        return niveaux.get(idxNiv).getClasses().get(classe);
    }

    Niveau getNiveaubyID(String niveau){
        int idxNiv = 0;
        char level = niveau.charAt(0);
        if (level == '6') {
            idxNiv = 0;
        } else if (level == '5') {
            idxNiv = 1;
        } else if (level == '4') {
            idxNiv = 2;
        } else if (level == '3') {
            idxNiv = 3;
        } else {
            return null;
        }
        return this.niveaux.get(idxNiv);
    }

    /**
     * Permet de loader le fichier json afin de créer les objets , si le fichier n'est pas connu on génére puis on insère dans le fichier
     * @param path
     */
    public void load(String path) {
        String json = "";
        try {
            try (FileReader reader = new FileReader(path);
                 BufferedReader br = new BufferedReader(reader)) {

                // read line by line
                String line;
                while ((line = br.readLine()) != null) {
                    json = json + line;
                }

            } catch (FileNotFoundException e) {
                System.out.println("Ficher pas trouvé , on genere le fichier");
                generate();
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Gson gson = new GsonBuilder().create();
        Niveau[] temp = gson.fromJson(json.toString(), Niveau[].class);

        this.niveaux = Arrays.asList(temp);


    }

    /**
     * Permet de generer tout le college et le mettre dans save.json s'il existe pas
     */
    private void generate() {
        this.niveaux = new ArrayList<>();
        niveaux.add(new Niveau("6ème"));
        niveaux.add(new Niveau("5ème"));
        niveaux.add(new Niveau("4ème"));
        niveaux.add(new Niveau("3ème"));
        Gson gson = new GsonBuilder().create();

        String json = gson.toJson(niveaux);
        Path path = null;
        try {
            path = Files.createTempFile("save", ".json");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (Files.exists(path)) {
            FileWriter fileWriter = null;

            try {
                fileWriter = new FileWriter("save.json");
            } catch (IOException e) {
                e.printStackTrace();
            }
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.print(json);
            printWriter.close();
            this.niveaux = niveaux;
        } else {
            System.out.println("Fichier n'existe pas");
        }
    }

    public List<Niveau> getNiveaux() {
        return niveaux;
    }
}
